export const pizzas = [
  {
    id: '1',
    name: 'pizza',
    ingredients: 'پیتزا مخلوط',
    count: 0,
    price: '8.30',
    image:
      'https://cdn11.bigcommerce.com/s-p6vajvx5jy/images/stencil/1280x1280/products/117/663/Meat_Lovers_2__84120.1556128972.jpg?c=2',
  },
  {
    id: '2',
    name: 'pizza',
    ingredients: 'پیتزا پنیری',
    count: 0,
    price: '7.10',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTOYeasmnCXW1bRyOZN015sspFejYJQsIyHEg&usqp=CAU',
  },
  {
    id: '3',
    name: 'pizza',
    ingredients: 'پنیری',
    count: 0,
    price: '5.10',
    image:
      'https://images.pexels.com/photos/825661/pexels-photo-825661.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '4',
    name: 'pizza',
    ingredients: 'قارچی',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/4394612/pexels-photo-4394612.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '5',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/4109077/pexels-photo-4109077.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '6',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2762942/pexels-photo-2762942.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '7',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1260968/pexels-photo-1260968.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '8',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/825661/pexels-photo-825661.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '9',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/842519/pexels-photo-842519.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '10',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/3915857/pexels-photo-3915857.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '11',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2120483/pexels-photo-2120483.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '12',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/5088687/pexels-photo-5088687.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '13',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/4193873/pexels-photo-4193873.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '14',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/5108601/pexels-photo-5108601.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '15',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/3731422/pexels-photo-3731422.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '16',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/5792325/pexels-photo-5792325.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '17',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/6287486/pexels-photo-6287486.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '18',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/7912401/pexels-photo-7912401.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '19',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2619967/pexels-photo-2619967.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '20',
    name: 'pizza',
    ingredients: 'پاستا',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/263041/pexels-photo-263041.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '21',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/208537/pexels-photo-208537.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '22',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2271194/pexels-photo-2271194.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '23',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1253737/pexels-photo-1253737.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '24',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2741457/pexels-photo-2741457.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '25',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2233348/pexels-photo-2233348.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '26',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1146760/pexels-photo-1146760.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '27',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2180875/pexels-photo-2180875.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
  },
  {
    id: '28',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1069449/pexels-photo-1069449.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '29',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2260561/pexels-photo-2260561.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '30',
    name: 'pizza',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/262977/pexels-photo-262977.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
];

// ============================= Bergers ====================================//

export const bergers = [
  {
    id: '1',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1893557/pexels-photo-1893557.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '2',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2282528/pexels-photo-2282528.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '3',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/3219547/pexels-photo-3219547.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '4',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2983098/pexels-photo-2983098.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '5',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/4109129/pexels-photo-4109129.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
  },
  {
    id: '6',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/4676441/pexels-photo-4676441.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '7',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/3826273/pexels-photo-3826273.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '8',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/3659861/pexels-photo-3659861.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '9',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2983102/pexels-photo-2983102.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '10',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/4676445/pexels-photo-4676445.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '11',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/4253708/pexels-photo-4253708.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '12',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/4409273/pexels-photo-4409273.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '13',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/4109268/pexels-photo-4109268.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '14',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/4871115/pexels-photo-4871115.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '15',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/6006599/pexels-photo-6006599.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '16',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/6006600/pexels-photo-6006600.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '17',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/6697455/pexels-photo-6697455.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '18',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/4809158/pexels-photo-4809158.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '19',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/5171016/pexels-photo-5171016.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '20',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/6242258/pexels-photo-6242258.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '21',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/7007929/pexels-photo-7007929.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '22',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1639557/pexels-photo-1639557.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '23',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1633578/pexels-photo-1633578.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '24',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/70497/pexels-photo-70497.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '25',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1108117/pexels-photo-1108117.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '26',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/580612/pexels-photo-580612.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '27',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/750075/pexels-photo-750075.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '28',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/327158/pexels-photo-327158.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '29',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2067403/pexels-photo-2067403.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '30',
    name: 'berger',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2983099/pexels-photo-2983099.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
];

// ============================= sushi ==============================================//

export const sushies = [
  {
    id: '1',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/6984182/pexels-photo-6984182.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '2',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/3298596/pexels-photo-3298596.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '3',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/3338499/pexels-photo-3338499.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '4',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/3304057/pexels-photo-3304057.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '5',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/3338496/pexels-photo-3338496.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '6',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/3338500/pexels-photo-3338500.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '7',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/4725576/pexels-photo-4725576.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '8',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/5208238/pexels-photo-5208238.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '9',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/6310247/pexels-photo-6310247.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '10',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/6144972/pexels-photo-6144972.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '11',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/5878433/pexels-photo-5878433.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '12',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/6531103/pexels-photo-6531103.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '13',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/6388704/pexels-photo-6388704.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '14',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/5677401/pexels-photo-5677401.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '15',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/6851406/pexels-photo-6851406.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '16',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/6851402/pexels-photo-6851402.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '17',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2098085/pexels-photo-2098085.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '18',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1148086/pexels-photo-1148086.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '19',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/271715/pexels-photo-271715.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '20',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/9210/food-japanese-food-photography-sushi.jpg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '21',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1108104/pexels-photo-1108104.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '22',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/684965/pexels-photo-684965.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '23',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2323398/pexels-photo-2323398.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '24',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/248444/pexels-photo-248444.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '25',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1052189/pexels-photo-1052189.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '26',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/359993/pexels-photo-359993.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '27',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1683545/pexels-photo-1683545.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '28',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/681586/sushi-japan-soya-rice-681586.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '29',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1148084/pexels-photo-1148084.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '30',
    name: 'sushi',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/3147493/pexels-photo-3147493.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
];

// =================== salad ======================================================//

export const salads = [
  {
    id: '1',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1059905/pexels-photo-1059905.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '2',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/257816/pexels-photo-257816.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '3',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/5938/food-salad-healthy-lunch.jpg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '4',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1213710/pexels-photo-1213710.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '5',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/434258/pexels-photo-434258.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '6',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1211887/pexels-photo-1211887.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '7',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/406152/pexels-photo-406152.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '8',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/806361/pexels-photo-806361.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '9',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2097090/pexels-photo-2097090.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '10',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/5916/food-salad-healthy-colorful.jpg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '11',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/764925/pexels-photo-764925.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '12',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2862154/pexels-photo-2862154.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '13',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/2116090/pexels-photo-2116090.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '14',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1152237/pexels-photo-1152237.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '15',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/628777/pexels-photo-628777.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '16',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1833333/pexels-photo-1833333.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '17',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/299352/pexels-photo-299352.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '18',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/709823/pexels-photo-709823.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '19',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1435893/pexels-photo-1435893.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '20',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1639556/pexels-photo-1639556.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '21',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1013518/pexels-photo-1013518.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '22',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/793759/pexels-photo-793759.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '23',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/566566/pexels-photo-566566.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '24',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/139751/pexels-photo-139751.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '25',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/5604/salad-leaf-leaves-green.jpg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '26',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1234535/pexels-photo-1234535.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '27',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/803963/pexels-photo-803963.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '28',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/3662123/pexels-photo-3662123.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '29',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1833334/pexels-photo-1833334.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
  {
    id: '30',
    name: 'salad',
    ingredients: 'گوشت  ماهی سالمون',
    count: 0,
    price: '9.55',
    image:
      'https://images.pexels.com/photos/1516415/pexels-photo-1516415.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500',
  },
];
